﻿void app()
{
    int numberTarget = (new Random()).Next(1, 11);
    const int maxLifes = 5;


    Console.WriteLine("Essayer de trouver le nombre magique entre 1 et 10");
    for (int i = 1; i <= maxLifes; i++)
    {
        Console.Write($"Essai n°{i}: ");
        string tryString = Console.ReadLine();

        try
        {
            int tryNumber = int.Parse(tryString);
            if (tryNumber < 1 || tryNumber > 10)
            {
                throw new Exception("Nombre pas bon");
            } else if(tryNumber == numberTarget) {
                Console.WriteLine($"Bravo vous avez trouvez le nombre magique {tryNumber} en {i} essai(s)");
                return;
            }
            else if(tryNumber > numberTarget)
            {
                Console.WriteLine($"Le nombre magique est plus petit que {tryNumber}");
            }
            else
            {
                Console.WriteLine($"Le nombre magique est plus grand que {tryNumber}");
            }
        }
        catch
        {
            Console.WriteLine("Veuillez saisir un entier valide entre 1 et 10");
        }
        finally
        {
            Console.WriteLine($"Il vous reste {maxLifes - i} essai(s)");
            Console.WriteLine();
        }
    }
    Console.WriteLine($"Désolé vous avez perdu. Le nombre magique était {numberTarget}");
}

app();